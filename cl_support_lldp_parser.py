#!/usr/bin/env python

import os
import sys
import glob
import shutil
import tarfile
import subprocess


# Get current dir
cwd = os.getcwd()

list_of_tarchives = glob.glob(cwd+'/*.txz')

for tarball in list_of_tarchives:
    print("Extracting: {}".format(tarball))
    try:
        subprocess.check_call(['tar','xf',tarball],)
    except:
        print("Could not extract: {}".format(tarball))
        sys.exit(1)
    # More Pythonic/Portable Way
    #    tar = tarfile.open(tarball)
    #    tar.extractall()
    #    tar.close()

lldp_dict = {}
eth0_map = {}
for tarball in list_of_tarchives:
    folder = tarball[:-4]
    print("folder is: {}".format(folder))
    f = open(folder + '/etc/hostname', 'r')
    hostname = f.readlines()[0][:-1]
    lldp_dict[hostname] = []
    print(hostname)
    # Find Eth0 IP
    with open(folder + '/Support/ip.addr', 'r') as ipaddrinfo:
        for line in ipaddrinfo.readlines():
            if "global eth0" in line:
                ip_and_mask = line.split()[1]
                ip_addr = ip_and_mask.split('/')[0]
                print("{} eth0 IP is: {}".format(hostname,ip_addr))
                eth0_map[hostname] = ip_addr
        if hostname not in eth0_map: eth0_map[hostname] = None
    # Find all the edges
    with open(folder + '/Support/lldpctl', 'r') as lldpinfo:
        interface = None
        remote_sys = None
        remote_interface = None
        for line in lldpinfo.readlines():
            if 'Interface:' in line:
                interface = line.split()[1][:-1]
            if 'SysName:' in line:
                remote_sys = line.split()[1]
            if 'PortID:' in line:
                remote_interface = line.split()[2]
                print("    {}:{} -- {}:{}".format(hostname,interface,remote_sys,remote_interface))
                lldp_dict[hostname].append({'localport':interface,'remotehost':remote_sys,'remoteport':remote_interface})

#import pprint
#pprint.pprint(lldp_dict)
for hostname in lldp_dict:
    hostname_link_set = {}
    for link in lldp_dict[hostname]:
        localport = link['localport']
        remotehost = link['remotehost']
        remoteport = link['remoteport']
        if link['localport'] in hostname_link_set:
            print("NOTE: {} interface '{}' appears to be dual-connected".format(hostname,link['localport']))
            print('         "{}":"{}" -- "{}":"{}"'.format(hostname,localport,remotehost,remoteport))
            print('         "{}":"{}" -- "{}":"{}"'.format(hostname,
                                                           localport,
                                                           hostname_link_set[link['localport']]['remotehost'],
                                                           hostname_link_set[link['localport']]['remoteport']))
        else:
            hostname_link_set[link['localport']] = link

already_done = []
dotfile_edges = []
dotfile_nodes = {}
# De-Dupe the edges and collect nodes
for hostname in lldp_dict:
    for link in lldp_dict[hostname]:
        localport = link['localport']
        remotehost = link['remotehost']
        remoteport = link['remoteport']
        if localport != 'vagrant' and remoteport != 'vagrant':
            if '"%s":"%s"'%(hostname,localport) not in already_done and '"%s":"%s"'%(remotehost,remoteport) not in already_done:
                dotfile_edges.append('    "{}":"{}" -- "{}":"{}"'.format(hostname,localport,remotehost,remoteport))
                already_done.append('"%s":"%s"'%(hostname,localport))
                already_done.append('"%s":"%s"'%(remotehost,remoteport))
                dotfile_nodes[hostname] = True
                dotfile_nodes[remotehost] = True
dotfile_stem = os.path.basename(cwd)
with open(dotfile_stem+".dot",'w') as dotfile:
    dotfile.write('graph dc1 {\n')
    for node in dotfile_nodes:
        if node in eth0_map and eth0_map[node]:
            dotfile.write('"{}" [function="leaf" mgmt_ip="{}"]\n'.format(node,eth0_map[node]))
        else:
            dotfile.write('"{}" [function="leaf" ]\n'.format(node))
    for edge in dotfile_edges:
        dotfile.write(edge+'\n')
    dotfile.write('}')

print("DOTFILE: {}.dot".format(dotfile_stem))

# Remove Untarred Files
for tarball in list_of_tarchives:
    folder = tarball[:-4]
    shutil.rmtree(folder)
